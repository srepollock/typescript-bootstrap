# TypeScript Bootstrap

A TypeScript bootstrap project: to start your project out without hassle. This project aims to give you the best tools you need to start your project on the right foot. When forking this project to start out your own unique idea, take some time to fill out the information here in this README. Remember, just like this README, your README.md file is the first contact to the end user. Most people don't read any more documentation (although I'm sure you'll have lots!) and really they should be able to get started with no more information than from here. Give them everything they need to get started on their own wonderful project.  
The following is simply a template of information to give. It is not final, it is not correct, it is simply a guidline of best practices.

Now remove this part and fill out the rest with your unique project information. Everything below can be removed, but some can be updated with your project specific information.
*Remember to give credit where it is due.*

> Created by [Spencer Pollock](@srepollock)
> [Contact me](http://spollock.ca)

---

# Project Title

[A short project description goes here. Up to a paragraph but no more.]

[![TypeScript](https://badges.frapsoft.com/typescript/version/typescript-next.svg?v=101)](https://github.com/ellerbrock/typescript-badges/)
[![Open Source Love](https://badges.frapsoft.com/os/v2/open-source.svg?v=103)](https://github.com/ellerbrock/open-source-badges/)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)

[![Build Status](https://travis-ci.org/srepollock/typescript-bootstrap.svg?branch=master)](https://travis-ci.org/srepollock/typescript-bootstrap) [![Coverage Status](https://coveralls.io/repos/github/srepollock/ts-bootstrap/badge.svg?branch=master)](https://coveralls.io/github/srepollock/ts-bootstrap?branch=master)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
Give examples
```

### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [NodeJS](http://www.nodejs.com)

## Contributing

Please read CONTRIBUTING.md for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **YOU** - [your profile](https://github.com/yourprofile)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
* **Billie Thompson** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)


> Created by [YOU!] (@[your-github-username]). 
> [Contact me](someone@example.com)
